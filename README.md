# dockrize elassandra

## build

```
docker build --tag erangaeb/elassandra:1.0 .
```

## run

```
docker run \
 -e CASSANDRA_BROADCAST_ADDRESS=10.4.1.26 \
 -p 9042:9042 \
 -p 9200:9200\
 erangaeb/elassandra:1.0
```

# seed node

```
docker run -d \
--name c1 \
-p 9160:9160 \
-p 9042:9042 \
-p 9200:9200 \
-p 9300:9300 \
-e LOCAL_JMX=no \
erangaeb/elassandra:6.2
```

# other nodes with seed parameter

```
docker run -d \
--name c2 \
-p 9170:9160 \
-p 9052:9042 \
-p 9201:9200 \
-p 9301:9300 \
-e CASSANDRA_SEEDS="$(docker inspect --format='{{ .NetworkSettings.IPAddress }}' c1)" \
-e LOCAL_JMX=no \
erangaeb/elassandra:6.2

docker run -d \
--name c3 \
-p 9180:9160 \
-p 9062:9042 \
-p 9202:9200 \
-p 9302:9300 \
-e CASSANDRA_SEEDS="$(docker inspect --format='{{ .NetworkSettings.IPAddress }}' c1)" \
-e LOCAL_JMX=no \
erangaeb/elassandra:6.2
```
