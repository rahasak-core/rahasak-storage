#!/bin/bash
CASSANDRA_HOME=/opt/elassandra
CASSANDRA_HOST=`hostname --ip-address`

sed -ri 's/^(# )?('"cluster_name"':).*/\2 '"$CLUSTER_NAME"'/' "$CASSANDRA_HOME/conf/cassandra.yaml"
sed -ri 's/^(# )?('"listen_address"':).*/\2 '"$CASSANDRA_HOST"'/' "$CASSANDRA_HOME/conf/cassandra.yaml"
sed -ri 's/^(# )?('"rpc_address"':).*/\2 '"$CASSANDRA_HOST"'/' "$CASSANDRA_HOME/conf/cassandra.yaml"
sed -ri 's/^(# )?('"write_request_timeout_in_ms"':).*/\2 '"$WRITE_TIME_OUT"'/' "$CASSANDRA_HOME/conf/cassandra.yaml"

# config broadcast address
if [ -z "$CASSANDRA_BROADCAST_ADDRESS" ]; then
    CASSANDRA_BROADCAST_ADDRESS=$CASSANDRA_HOST
fi
sed -ri 's/^(# )?('"broadcast_address"':).*/\2 '"$CASSANDRA_BROADCAST_ADDRESS"'/' "$CASSANDRA_HOME/conf/cassandra.yaml"
sed -ri 's/^(# )?('"broadcast_rpc_address"':).*/\2 '"$CASSANDRA_BROADCAST_ADDRESS"'/' "$CASSANDRA_HOME/conf/cassandra.yaml"

# config cassandra seeds
if [ -z "$CASSANDRA_SEEDS" ]; then
	CASSANDRA_SEEDS=$CASSANDRA_BROADCAST_ADDRESS
fi
sed -ri 's/(- seeds:).*/\1 "'"$CASSANDRA_SEEDS"'"/' "$CASSANDRA_HOME/conf/cassandra.yaml"

# configure JMX_PORT
if [ -z "$JMX_PORT" ]; then
    JMX_PORT=$JMX_PORT
fi
sed -ri 's/(JMX_PORT=).*/\1"'"$JMX_PORT"'"/' "$CASSANDRA_HOME/conf/cassandra-env.sh"

# Set JVM SSL encryption options for Cassandra
if [ "$CASSANDRA_ENABLE_JMX_SSL" == "true" ]; then
    sed -ie 's/#JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl=true"/JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl=true"/g' $CASSANDRA_HOME/conf/cassandra-env.sh
    sed -ie 's/#JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl.need.client.auth=true"/JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl.need.client.auth=true"/g' $CASSANDRA_HOME/conf/cassandra-env.sh
    sed -ie 's/#JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl.enabled.protocols=<enabled-protocols>"/JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl.enabled.protocols=TLSv1.2,TLSv1.1,TLSv1"/g' $CASSANDRA_HOME/conf/cassandra-env.sh
    sed -ie 's/#JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl.enabled.cipher.suites=<enabled-cipher-suites>"/JVM_OPTS="$JVM_OPTS -Dcom.sun.management.jmxremote.ssl.enabled.cipher.suites=TLS_RSA_WITH_AES_256_CBC_SHA"/g' $CASSANDRA_HOME/conf/cassandra-env.sh
    sed -ie "s/#JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.keyStore=\/path\/to\/keystore\"/JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.keyStore=\/etc\/ssl\/cassandra-server-keystore\.jks\"/g" $CASSANDRA_HOME/conf/cassandra-env.sh
    sed -ie "s/#JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.keyStorePassword=<keystore-password>\"/JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.keyStorePassword=${CASSANDRA_KEYSTORE_PASSWORD}\"/g" $CASSANDRA_HOME/conf/cassandra-env.sh
    sed -ie "s/#JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.trustStore=\/path\/to\/truststore\"/JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.trustStore=\/etc\/ssl\/generic-server-truststore\.jks\"/g" $CASSANDRA_HOME/conf/cassandra-env.sh
    sed -ie "s/#JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.trustStorePassword=<truststore-password>\"/JVM_OPTS=\"\$JVM_OPTS -Djavax.net.ssl.trustStorePassword=${CASSANDRA_TRUSTSTORE_PASSWORD}\"/g" $CASSANDRA_HOME/conf/cassandra-env.sh
fi

# Set the Client encryption options in the Cassandra configuration file
if [ "$CASSANDRA_ENABLE_CLIENT_SSL" == "true" ]; then
    # grab the line number of the 'client_encryption_options' property then iterate down through the file until the
    # first empty line is reached. This will be the end of the block containing all the properties for
    # 'client_encryption_options'.
    start_line_number=$(grep -n "client_encryption_options:" "$CASSANDRA_HOME/conf/cassandra.yaml" | cut -d':' -f1)
    count=${start_line_number}
    line=$(sed "${count}q;d" "$CASSANDRA_HOME/conf/cassandra.yaml")

    while [ "${line}" != "" ]
    do
      ((count++))
      line=$(sed "${count}q;d" "$CASSANDRA_HOME/conf/cassandra.yaml")
    done

    end_line_number=${count}

    for key_val in \
        "enabled:true" \
        "keystore:\/etc\/ssl\/cassandra\-server\-keystore\.jks" \
        "keystore_password:${CASSANDRA_KEYSTORE_PASSWORD}" \
        "truststore:\/etc\/ssl\/generic\-server\-truststore\.jks" \
        "truststore_password:${CASSANDRA_TRUSTSTORE_PASSWORD}"
    do
      property=$(echo ${key_val} | cut -d':' -f1)
      value=$(echo ${key_val} | cut -d':' -f2)
      sed -ie "${start_line_number},${end_line_number} s/\(#\ \)\{0,1\}\(${property}:\).*/\2 ${value}/" "$CASSANDRA_HOME/conf/cassandra.yaml"
    done
fi

# set logging
# ROOT level to INFO
sed -ri 's/(root level=).*/\1"'INFO'">/' "$CASSANDRA_HOME/conf/logback.xml"

exec "$@"
