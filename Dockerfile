FROM ubuntu:18.04

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# explicitly set user/group
RUN groupadd -r cassandra --gid=999 && useradd -r -g cassandra --uid=999 cassandra

# install required packages
# install wget/curl
# python cassandra driver
RUN apt-get update -y
RUN apt-get install -y python 
RUN apt-get install -y python-pip
RUN apt-get install -y software-properties-common
RUN apt-get install -y curl
RUN apt-get install -y wget
RUN pip install cassandra-driver

# install java
RUN apt-get install -y openjdk-8-jdk
RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf /var/cache/oracle-jdk8-installer

# set JAVA_HOME
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

# install elassandra
ENV ELASSANDRA_VERSION 6.8.4.3
RUN wget https://github.com/strapdata/elassandra/releases/download/v$ELASSANDRA_VERSION/elassandra-$ELASSANDRA_VERSION.tar.gz
RUN tar -xzf elassandra-$ELASSANDRA_VERSION.tar.gz -C /opt
RUN mv /opt/elassandra-$ELASSANDRA_VERSION /opt/elassandra
RUN rm elassandra-$ELASSANDRA_VERSION.tar.gz

# install ssh server
RUN apt-get update
RUN apt-get -y install sudo
RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:root' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# envirounments
ENV CLUSTER_NAME rahasak
ENV WRITE_TIME_OUT 20000
ENV JMX_PORT 7199

# scripts/dirs
ADD scripts /usr/local/bin/
RUN mkdir -p /var/log/cassandra
RUN chown -R cassandra:cassandra /var/log/cassandra
RUN chmod -R 775 /var/log/cassandra
RUN chown -R cassandra:cassandra /opt/elassandra
RUN chown -R cassandra:cassandra /usr/local/bin

# data dir
VOLUME ["/opt/elassandra/data"]

RUN usermod -aG sudo cassandra
RUN echo "cassandra:cassandra" | chpasswd

# now user is cassandra
USER cassandra

# 7000: ipc; 7001: tls ipc; 7199: jmx; 9042: cql; 9160: thrift; 9200|9300: elasticsearch
EXPOSE 22 7000 7001 7199 9042 9160 9200 9300
ENTRYPOINT ["/usr/local/bin/start.sh"]
